package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class loginsteps {
	ChromeDriver driver;
	
	@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromerdriver.exe");
		driver=new ChromeDriver();    
	}

	@Given("load the url")
	public void loadTheUrl() {
	    driver.get("http://leaftaps.com/opentaps/control/main");
	    throw new PendingException();
	}

	@Given("enter the username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String uname) {
		driver.findElementById("username").sendKeys("uname");
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Given("enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		driver.findElementById("password").sendKeys("pwd");
		throw new PendingException();
	}

	@When("click on the login button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("verify the login is success")
	public void verifyTheLoginIsSuccess() {
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("click on crm\\/sfa")
	public void clickOnCrmSfa() {
		
		driver.findElementByLinkText("CRM/SFA").click();
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("click on createlead")
	public void clickOnCreatelead() {
		driver.findElementByLinkText("Create Lead").click();
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("enter the compname")
	public void enterTheCompname() {
		
		driver.findElementById("createLeadForm_companyName").sendKeys("HCL");
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("enter the firstname")
	public void enterTheFirstname() {
		driver.findElementById("createLeadForm_firstName").sendKeys("Kalaise");
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@When("enter the lastname")
	public void enterTheLastname() {
		driver.findElementById("createLeadForm_lastName").sendKeys("S");
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}

	@Then("verify create lead success")
	public void verifyCreateLeadSuccess() {
		
		driver.findElementByClassName("smallSubmit").click();
	    // Write code here that turns the phrase above into concrete actions
	    throw new PendingException();
	}


		
		
	}


